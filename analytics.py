import os

import numpy as np
import pandas as pd
from scipy.stats import shapiro, f


class Analytics:
    """
    Доказательство различия между новостными таймфреймами и неновостными
    """
    def hourly_price(self):
        directory = r'C:\Users\Gleb\Desktop\stocks'
        main_df = pd.DataFrame(columns=['<TICKER>', '<PER>', '<OPEN>', '<HIGH>', '<LOW>', '<CLOSE>', '<VOL>', 'Date'])
        for file in os.listdir(directory):
            if file != 'news.csv':
                df = pd.read_csv(directory + '\\' + file)
                df['<DATE>'] = df['<DATE>'].apply(str)
                df['YEAR'] = '20' + df['<DATE>'].str.slice(-2)
                df['MON'] = df['<DATE>'].str.slice(-4, -2)
                df['DAY'] = df['<DATE>'].str.slice(stop=-4)
                df['Date'] = pd.to_datetime(df['YEAR'] + '/' + df['MON'] + '/' + df['DAY'] + ' ' + df['<TIME>'])
                df = df.drop(['<DATE>', 'YEAR', 'MON', 'DAY', '<TIME>'], axis=1)
                main_df = main_df.append(df)

        main_df = main_df.reset_index()

        news = pd.read_csv(r'C:\Users\Gleb\Desktop\stocks\news.csv', sep=';', index_col=0)
        news = news.drop([news.index[-1]])
        news['YEAR'], news['MON'], news['DAY'] = '', '', ''
        translate = {'фев': '02', 'янв': '01', 'дек': '12'}
        fixing_tickers = {'Tesla': 'MOEX.TSLA-RM:TQBD',
                          'VK': 'MOEX.VKCO:TQBR',
                          'PEP': 'MOEX.PEP-RM:FQBR',
                          'AMZN': 'AMZN-RM'}

        for idx in news.index:
            if news['Date'][idx][-2:].isdigit():
                news['YEAR'][idx] = '20' + news['Date'][idx][-2:]
                news['MON'][idx] = translate[news['Date'][idx][3:6]]
            else:
                news['YEAR'][idx] = '2022'
                news['MON'][idx] = translate[news['Date'][idx][-3:]]
            news['DAY'][idx] = news['Date'][idx][:2]
            if news['Company'][idx] in fixing_tickers:
                news['Company'][idx] = fixing_tickers[news['Company'][idx]]

        news['Date'] = pd.to_datetime(news['YEAR'] + '/' + news['MON'] + '/' + news['DAY'] + ' ' + news['Time'])
        news = news.drop(['YEAR', 'MON', 'DAY'], axis=1)

        results_news = np.zeros(news.shape[0], )
        i = 0
        for idx in news.index:
            cur_df = main_df[main_df['<TICKER>'] == news['Company'][idx]]
            cur_df['time_diff'] = news['Date'][idx] - cur_df['Date']
            cur_df['abs_time_diff'] = abs(cur_df['time_diff'])
            cur_df = cur_df.sort_values(by='abs_time_diff', ascending=True)

            n = 0
            while n < len(cur_df):
                if cur_df['Date'][cur_df.index[n]] < news['Date'][idx]:
                    starting_point = cur_df['Date'][cur_df.index[n]]
                    starting_price = cur_df['<CLOSE>'][cur_df.index[0]]
                    break
                n += 1

            end_point = starting_point + pd.Timedelta(2, unit='H')
            cur_df['abs_time_diff'] = abs(end_point - cur_df['Date'])
            cur_df = cur_df.sort_values(by='abs_time_diff', ascending=True)
            end_price = cur_df['<CLOSE>'][cur_df.index[0]]
            change = (end_price / starting_price - 1) * 100
            results_news[i] = change
            i += 1

        results_random = np.zeros(news.shape[0], )
        j = 0
        random_delta = list(range(6, 14))
        for idx in news.index:
            news['Date'][idx] += pd.Timedelta(np.random.choice(random_delta), unit='H')

            cur_df = main_df[main_df['<TICKER>'] == news['Company'][idx]]
            cur_df['time_diff'] = news['Date'][idx] - cur_df['Date']
            cur_df['abs_time_diff'] = abs(cur_df['time_diff'])
            cur_df = cur_df.sort_values(by='abs_time_diff', ascending=True)

            n = 0
            while n < len(cur_df):
                if cur_df['Date'][cur_df.index[n]] < news['Date'][idx]:
                    starting_point = cur_df['Date'][cur_df.index[n]]
                    starting_price = cur_df['<CLOSE>'][cur_df.index[0]]
                    break
                n += 1

            end_point = starting_point + pd.Timedelta(2, unit='H')
            cur_df['abs_time_diff'] = abs(end_point - cur_df['Date'])
            cur_df = cur_df.sort_values(by='abs_time_diff', ascending=True)
            end_price = cur_df['<CLOSE>'][cur_df.index[0]]
            change = (end_price / starting_price - 1) * 100
            results_random[j] = change
            j += 1

        # F = np.var(results_news) / np.var(results_random)
        # print(1 - f.cdf(F, len(results_news) - 1, len(results_random) - 1))

    """
    Выделение самых часто упоминаемых (в новостях) компаний
    """
    def top_x(self):
        df = pd.read_csv(r'C:\Users\Gleb\Documents\course-work\data.txt', sep=';', encoding='utf-16',
                         names=['Article', 'Date', 'Ticker'])
        best_100 = df.Ticker.value_counts().head(11)
        s = 0
        for i in best_100.index:
            print(i)
            s += best_100[i]
