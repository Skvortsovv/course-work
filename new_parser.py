import json
import os

from bs4 import BeautifulSoup
import requests
from datetime import datetime, timedelta
import time


class Parser:
    def __init__(self, apis):
        self.apis = apis

    def parse(self):
        today = datetime(day=12, month=2, year=2022, hour=0, minute=0, second=0)
        year_ago = today - timedelta(days=365)
        api_index = 0

        def data_formatting(obj):
            return obj if obj > 9 else '0' + str(obj)

        inp = input('If you want to reset data press Y\n')
        if inp.lower() != 'y':
            return

        companies_l = [i[:-4] for i in os.listdir('C:/Users/Gleb/Desktop/stocks')]
        with open(r'./new_data.txt', 'w', encoding="utf-16") as doc:
            for company in companies_l:
                while today > year_ago:
                    today_string = f'{today.year}-{data_formatting(today.month)}-{data_formatting(today.day)}T' \
                                   f'{data_formatting(today.hour)}:{data_formatting(today.minute)}:{data_formatting(today.second)} '
                    api = self.apis[api_index]
                    request = requests.get(f'https://api.marketaux.com/v1/news/all?symbols={company}&filter_entities=true'
                                           f'&domains=forbes.com&language=en&api_token={api}&published_before={today_string}')

                    page = json.loads(BeautifulSoup(request.text, 'html.parser').text)

                    if "error" in page:
                        if page['error']['code'] == 'usage_limit_reached':
                            if api_index + 1 < len(self.apis):
                                api_index += 1
                                continue
                            else:
                                print('The last one:', api)
                                break
                        elif page['error']['code'] == 'rate_limit_reached':
                            time.sleep(60)
                            print("I'm sleeping")
                            continue
                    try:
                        page["data"]
                    except KeyError:
                        print(page)

                    for n_article in range(len(page)):
                        tickers = [i['symbol'] for i in page['data'][n_article]['entities']]
                        sentiment = [i['sentiment_score'] for i in page['data'][n_article]['entities']]

                        for ticker in range(len(tickers)):
                            observation = dict()

                            observation['title'] = page['data'][n_article]['title']
                            observation['ticker'] = tickers[ticker]
                            observation['sentiment'] = str(sentiment[ticker])

                            published_at = page['data'][n_article]['published_at']
                            publ_date, publ_time = published_at.split('T')
                            publ_time = publ_time[:publ_time.index('.')]
                            observation['date_published'] = publ_date + ' ' + publ_time

                            doc.write(';'.join(list(observation.values())) + '\n')

                            if n_article == len(page) - 1:
                                today = datetime.strptime(publ_date + ' ' + publ_time, '%Y-%m-%d %H:%M:%S')

# https://api.marketaux.com/v1/news/all?symbols=TSLA&filter_entities=true&language=en&
# api_token=M2OmtPWXH7uoxe0tyeB58eV3VhlLydNAYsnqebeB&published_before=2022-12-02&domains=forbes.com
