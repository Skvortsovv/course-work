import os
import pandas as pd

import warnings

warnings.filterwarnings("ignore")


class Label:
    def __init__(self, data_source):
        self.data_source = data_source

    def label(self, prices_path):
        ticker_list = [i[:-4] for i in os.listdir(prices_path)]

        news_df = pd.read_csv(self.data_source, encoding='utf-16', names=['Article', 'Ticker', 'Sentiment', 'Date'],
                              sep=';')

        news_df = news_df[news_df.Ticker.isin(ticker_list)]
        news_df['Date'] = pd.to_datetime(news_df['Date'], format='%Y-%m-%d %H:%M:%S')
        news_df['Change'] = 0.
        news_df = news_df.reset_index().drop(["index"], axis=1)

        for article_idx in news_df.index:
            print(news_df.Ticker[article_idx])
            prices_df = pd.read_csv(f'C:/Users/Gleb/Desktop/stocks/{news_df.Ticker[article_idx]}.csv')
            prices_df['<DATE>'] = prices_df['<DATE>'].astype(str)
            prices_df['<TIME>'] = prices_df['<TIME>'].astype(str)

            prices_df['Date_normalized'] = prices_df['<DATE>'].str.slice(0, 4) + '/' + \
                                           prices_df['<DATE>'].str.slice(4, 6) + '/' + \
                                           prices_df['<DATE>'].str.slice(6, 8) + ' ' + \
                                           prices_df['<TIME>'].apply(lambda x: self.time_processing(x))
            prices_df['Date_normalized'] = pd.to_datetime(prices_df['Date_normalized'])

            try:  # Новость вышла раньше, чем наш массив данных о ценах
                start_price = self.nearest_time(prices_df, news_df['Date'][article_idx], True)
                article_time_shifted = news_df['Date'][article_idx] + pd.Timedelta(2, unit='H')
                end_price = self.nearest_time(prices_df, article_time_shifted, False)
            except IndexError:
                news_df['Change'][article_idx] = None
                continue

            change = round((end_price / start_price - 1) * 100, 3)
            news_df['Change'][article_idx] = change

        news_df = news_df[news_df['Change'].isnull() == False]
        news_df.Change = news_df.Change.apply(lambda x: 0 if abs(x) < 0.5 else 1 if x > 0 else -1)

        news_df.to_csv('./new_data.txt', encoding='utf-16', index=False, header=False, sep=';')

    def nearest_time(self, price_df, article_time, is_start):
        price_df['abs_time_diff'] = abs(price_df.Date_normalized - article_time)
        prices_df = price_df.sort_values(by='abs_time_diff', ascending=True)
        if is_start:
            price = prices_df[prices_df['Date_normalized'] < article_time].iloc[0]['<CLOSE>']
        else:
            price = prices_df[prices_df['Date_normalized'] > article_time].iloc[0]['<CLOSE>']
        return price

    def time_processing(self, x):
        if len(x) == 6:
            return x[0:2] + ':' + x[2:4]
        elif len(x) == 5:
            return '0' + x[0:1] + ':' + x[1:3]
        elif len(x) == 4:
            return '00' + ':' + x[0:2]
        elif len(x) == 3:
            return '00' + ':0' + x[0:1]
        else:
            return '00:00'
