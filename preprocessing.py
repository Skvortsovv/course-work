import pandas as pd
from nltk.corpus import stopwords
from pymystem3 import Mystem
from string import punctuation
from sklearn.feature_extraction.text import TfidfVectorizer


class Preprocessing:
    def __init__(self):
        self.russian_stopwords = stopwords.words("english")
        self.mystem = Mystem()

    def preprocess(self, df_path):
        # Дамми на тикеры
        # TODO: Сделать категорию Other
        df = pd.read_csv(df_path, encoding='utf-16',
                         names=['Article', 'Ticker', 'Sentiment', 'Date', 'Change'],
                         sep=';')
        # dummies = pd.get_dummies(df.Ticker)
        # df = pd.concat([dummies, df], axis=1).drop(['Ticker'], axis=1)

        # Лемматизация
        df.Article = [self.lemmatize(text) for text in df.Article]

        # Конвертация по TF-IDF
        tfidfconverter = TfidfVectorizer(min_df=5, max_df=0.7)
        X = pd.DataFrame(tfidfconverter.fit_transform(df.Article).toarray())
        X.columns = tfidfconverter.get_feature_names()
        df = pd.concat([X, df], axis=1).drop(['Article', 'Date'], axis=1)

        df.to_csv(r'./data_lemmatized.txt', encoding='utf-16', index=False)

    def lemmatize(self, text):
        for pattern in ["’", "‘", '"', '“', '"', '”', '”"', '—']:
            text = text.replace(pattern, "")

        text = self.mystem.lemmatize(text.lower())
        text = [token for token in text if token not in self.russian_stopwords \
                and token != " " and token.strip() not in punctuation]
        return " ".join(text)
