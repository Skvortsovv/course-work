import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import SVC


class Model:
    def model(self, df_path):
        df = pd.read_csv(df_path, encoding='utf-16', header=0)
        X = df.drop(['Change', 'Ticker'], axis=1)
        y = df.Change
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=0)

        svm = SVC()
        svm.fit(X_train, y_train)
        svm_pred = svm.predict(X_test)

        results = svm_pred == y_test
        print(sum(svm_pred) / len(results))

        forest = RandomForestClassifier(n_estimators=1000, random_state=0)
        forest.fit(X_train, y_train)
        forest_pred = forest.predict(X_test)

        results = forest_pred == y_test
        print(sum(forest_pred) / len(results))

        feature_names = [f"feature {i}" for i in range(X.shape[1])]
        importances = forest.feature_importances_
        forest_importances = pd.Series(importances, index=feature_names)
        print(forest_importances.sort_values(axis=0, ascending=False))
