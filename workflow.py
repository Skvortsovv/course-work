from new_parser import Parser
from label import Label
from preprocessing import Preprocessing
from model import Model

parser = Parser(['M2OmtPWXH7uoxe0tyeB58eV3VhlLydNAYsnqebeB',
                 '947aYvG5BgJmCkYS6T19dlpnqUPQGxBL4mXXDyfb',
                 'ldHqy7T63yQer4eLnpNpmMvqLTfmVbOR2BJJTGBN',
                 'eZ2EmEGu6QKcQr1gRdweafU19mEAvWKRprnTjmeu',
                 'kbpWYZCn7iZWa21hn275SYkLqyhqC2IBTioGFs6p',
                 '8Ngx85T78mq174zJLHMcGLnilbkEmYjFhs6yH3eF',
                 'aLd5uNNgMhAXby54qkPQwxuHtkC72V7sLyO5vZMF',
                 'whUknHszQSytsotMLbURjqzxpJrvgu2iwqUIrVIt',
                 'DzDFeYMe1uV8NcGauIqd2Ml7M5fnujkOoW1zWCqF',
                 'PbWkin4JMZJa2IDEaQVp0YLXyGRfqjSgLqDXfr24',
                 'c1qVRxzo0rcLNQ5jpWROujVg5YslQ7BnwD00XH4t',
                 'vqJwK2S0IaVcZ4ZiQaPtMLsvuSPmCS1vx9rIdgzy',
                 'AV37Y3cGjniPEQaoGubDD1JItZhUb7uSixxutUv0',
                 'V32Hpv1qxzNL8JHZGPQ4sdNuLSjUs8f112o2FZNt'])
parser.parse()

labeller = Label('./new_data.txt')
labeller.label(r'C:/Users/Gleb/Desktop/stocks')

preprocess = Preprocessing()
preprocess.preprocess('./new_data.txt')

model = Model()
model.model('./data_lemmatized.txt')

