import requests
from bs4 import BeautifulSoup
from datetime import date, timedelta
import json


class Parser:
    def parse(self):
        inp = input('If you want to reset data press Y\n')
        if inp.lower() == 'y':
            days_am = int(input('How many days for parsing?\n'))
            today = date(day=12, month=2, year=2022)
            year_ago = today - timedelta(days=days_am)
            print('Parsing...')

            with open(r'./data.txt', 'w', encoding="utf-16") as doc:
                for day in range((today - year_ago).days):
                    to_date = (today - timedelta(days=day))
                    to_date = to_date.strftime("%d.%m.%Y")

                    request = requests.get(
                        f"https://www.rbc.ru/v10/search/ajax/?project=quote&dateFrom={to_date}&dateTo={to_date}&offset=0&limit=100")
                    print(f"https://www.rbc.ru/v10/search/ajax/?project=quote&dateFrom={to_date}&dateTo={to_date}&offset=0&limit=100")
                    page = json.loads(BeautifulSoup(request.text, 'html.parser').text)['items']
                    for article in page:
                        observation = {
                            'title': article['title'],
                            'date_time': str(article['publish_date_t'])
                        }
                        tick_soup = BeautifulSoup(requests.get(article['fronturl']).text, 'html.parser')
                        try:
                            blocks = tick_soup.find_all('div', class_='q-item__company q-item__company_article')[0] \
                                .find_all('span', class_='q-item__company__name-short')
                        except IndexError:
                            continue
                        for ticker in blocks:
                            observation['tickers'] = ticker.find('span',
                                                                 class_='q-item__company__inner').text.strip().replace(
                                ' ', '-')

                            doc.write(';'.join(list(observation.values())) + '\n')


parser = Parser()
parser.parse()
